///<reference path="../../../typings/index.d.ts" />

import {RARequestsSender} from "../../main/js/RARequestSender";

describe("RARequestSender Tests", function() {
    
    beforeEach(function() {
        spyOn(chrome.runtime.onConnect, "addListener");
        this.rarObject = new RARequestsSender("serverLocation");
    });

    it("should create an object and call register", function() {
        expect(this.rarObject.getApiLocation()).toEqual("serverLocation");
        expect(chrome.runtime.onConnect.addListener).toHaveBeenCalled();
        expect(this.rarObject.isSent()).toBeFalsy();
    });

    it("should have an api_location set", function() {
        this.rarObject = new RARequestsSender(null);
        let returnValue = this.rarObject.sendRequest("table", {});
        
        expect(returnValue).toEqual(undefined);
    });

    /**
     * A test for all different status.
     * @type {{status: number}[]}
     */
    let testArray = [
        {status: 200},
        {status: 201},
        {status: 202}
    ];

    testArray.forEach( function(item) {
        it("should send the request and set send to true if it succeeds.", function () {
            XMLHttpRequest.prototype.status = item.status;
            XMLHttpRequest.prototype.readyState = 4;
            this.rarObject.sendRequest("table", {});
            
            expect(this.rarObject.isSent()).toBeTruthy();
        });
    });

    it("gulp should send the request and set send to false if it fails.", function() {
        XMLHttpRequest.prototype.status = 400;
        XMLHttpRequest.prototype.readyState = 4;
        this.rarObject.sendRequest("table", {});
        
        expect(this.rarObject.isSent()).toBeFalsy();
    });

    it("should allow setting the api_location", function() {
        this.rarObject.setApiLocation("newLocation");

        expect(this.rarObject.getApiLocation()).toEqual("newLocation");
    });
});