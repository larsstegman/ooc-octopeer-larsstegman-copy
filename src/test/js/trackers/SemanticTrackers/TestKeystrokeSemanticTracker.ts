///<reference path="../../../../../typings/index.d.ts" />

import {SemanticTrackerTest} from "./TestSemanticTracker";
import {KeystrokeSemanticTracker} from "../../../../main/js/trackers/SemanticTrackers/KeystrokeSemanticTracker";

SemanticTrackerTest(KeystrokeSemanticTracker);

describe("The keystroke semantic tracker", function () {

    beforeEach(function () {
        jasmine.clock().install();
        jasmine.clock().mockDate();
        
        this.tracker = new KeystrokeSemanticTracker();
        this.collector = jasmine.createSpyObj("collector", ["sendMessage"]);
        this.element = jasmine.createSpyObj("elem", ["addEventListener"]);
        this.tracker.withCollector(this.collector);
    });

    afterEach(function () {
        jasmine.clock().uninstall();
    });

    it("should register the element correctly.", function () {
        this.element.addEventListener.and.callFake((_: string, callback: any) => {
            callback();
        });
        
        this.tracker.registerElement(this.element, "Add reaction");

        expect(this.collector.sendMessage).toHaveBeenCalledWith(jasmine.objectContaining({
            data: jasmine.objectContaining({
                event_type: 101,
                element_type: 110
            })
        }));
    });
});