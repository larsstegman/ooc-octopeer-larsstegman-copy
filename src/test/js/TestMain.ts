/// <reference path="../../../typings/index.d.ts" />

import {Main} from "../../main/js/Main";

describe("The Main", function() {

    beforeEach(function () {
        this.main = new Main();
    });

    it("Should not allow more than a single TrackingCollector", function () {
        this.main.declareTrackingCollector((_: SessionDataGatherer) => <TrackingCollector>{});
        
        expect(() => {
            this.main.declareTrackingCollector((_: SessionDataGatherer) => <TrackingCollector>{});
        }).toThrowError();
    });

    it("Should not allow more than a single Session Data Gatherer", function () {
        this.main.declareSessionDataGatherer(() => <SessionDataGatherer>{});
        
        expect(() => {
            this.main.declareSessionDataGatherer(() => <SessionDataGatherer>{});
        }).toThrowError();
    });

    it("Should not allow more than a single Semantic Mapping", function () {
        this.main.declareSemanticMappings([]);
        
        expect(() => {
            this.main.declareSemanticMappings([]);
        }).toThrowError();
    });

    it("Should allow defining a few trackers.", function() {
        let tracker1 = jasmine.createSpy("Tracker");
        let tracker2 = jasmine.createSpy("Tracker");
        this.main.declareTracker({
            tracker: tracker1,
            setting: {name: "x", def: true}
        });
        
        this.main.declareTracker({
            tracker: tracker2,
            setting: {name: "y", def: false}
        });
    });
});

describe("The done function in Main.", function() {

    beforeEach(function () {
        this.main = new Main();
        this.tracker1 = jasmine.createSpy("Tracker");
        this.tracker2 = jasmine.createSpy("Tracker");
        this.sessiondata = jasmine.createSpyObj("SessionDataGatherer", ["getSessionData"]);
        this.collector = jasmine.createSpyObj("Collector", ["isReadyToSend", "sendMessage"]);
        this.main.declareTracker({
            tracker: this.tracker1,
            setting: {name: "x", def: true}
        });

        this.main.declareTracker({
            tracker: this.tracker2,
            setting: {name: "y", def: false}
        });
        this.main.declareSemanticMappings([]);
        this.main.declareSessionDataGatherer(() => this.sessiondata);
        this.main.declareTrackingCollector((_: SessionDataGatherer) => this.collector);
    });

    it("should correctly infer default settings.", function(done) {
        this.collector.isReadyToSend.and.returnValue(true);

        spyOn(chrome.storage.sync, "get").and.callFake(
            function(settings: { [key: string]: boolean },
                     activation: (settings: { [key: string]: boolean }) => void ) {
                expect(settings["x"]).toBeTruthy();
                expect(settings["y"]).toBeFalsy();
                done();
            });

        this.main.done();
    });

    it("should enable trackers on basis of their setting.", function(done) {
        this.collector.isReadyToSend.and.returnValue(true);
        const _current = this;

        spyOn(chrome.storage.sync, "get").and.callFake(
            function(settings: { [key: string]: boolean },
                     activation: (settings: { [key: string]: boolean }) => void ) {
                // User changed settings, different from defaults!
                settings["x"] = true;
                settings["y"] = false;
                activation(settings);
                expect(_current.tracker1).toHaveBeenCalled();
                expect(_current.tracker2).not.toHaveBeenCalled();
                done();
            });

        this.main.done();
    });

    it("should check if a collector got the data it required before registering trackers", function() {
        this.collector.isReadyToSend.and.returnValue(false);
        expect(() => this.main.done()).toThrowError();
    });

    it("should check if a collector is defined before registering trackers.", function() {
        this.collector = null;
        expect(() => this.main.done()).toThrowError();
    });
});